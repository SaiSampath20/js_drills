function problem2(inventory){
    arr = [];
    if( inventory == undefined || inventory.length == 0 )
        return arr;
    let i = inventory.length - 1;
    arr.push(`Last car is a ${inventory[i].car_make} ${inventory[i].car_model}`)
    return arr;
}

module.exports = problem2;