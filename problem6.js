function problem6(inventory){
    arrBMWAndAudi = [];
    if( inventory == undefined || inventory.length == 0 )
        return arrBMWAndAudi;
    for(let i = 0; i < inventory.length; i++){
        if( inventory[i].car_make == "Audi" || inventory[i].car_make == "BMW" )
            arrBMWAndAudi.push( inventory[i] )
    }
    return arrBMWAndAudi;
}

module.exports = problem6;