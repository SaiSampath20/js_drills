function problem5(inventory, arrCarYear){
    less = [];
    if( inventory == undefined || inventory.length == 0 || arrCarYear == undefined )
        return less;
    for(let i = 0; i < arrCarYear.length; i++){
        if( arrCarYear[i] < 2000 )
            less.push( arrCarYear[i] );
    }
    return less;
}

module.exports = problem5;