function problem4(inventory){
    arrCarYear = [];
    if( inventory == undefined || inventory.length == 0 )
        return arrCarYear;
    for(let i = 0; i < inventory.length; i++){
        arrCarYear.push( inventory[i].car_year );
    }
    return arrCarYear;
}

module.exports = problem4;