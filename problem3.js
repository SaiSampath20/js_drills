function problem3(inventory){
    if( inventory == undefined || inventory.length == 0 )
        return [];
    inventory.sort( (a, b) => ( a.car_model < b.car_model ) ? -1 : 1 )
    return inventory;
}

module.exports = problem3;